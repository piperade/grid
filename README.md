# Piperade Grid
## A simple CSS grid powered by flexbox

[Lire en français](README_FR.md)
### Installation
Download last version : [CSS (Compiled)](https://framagit.org/piperade/grid/-/jobs/artifacts/master/download?job=compile), [SCSS (Source)](https://framagit.org/piperade/grid/-/raw/master/grille.scss?inline=false)  

### Available classes
* `container`, for contain all page
* `row`, for contain columns
* `col`, for contain content
* `center`, center align the content of a row
* `start`, start align the content of a row
* `end`, end align the content of a row

### Responsive Web Design

#### Several screen sizes :
* S : <= 600px (Mobile Phone)
* M : > 600px (Tablet)
* L : > 992px (Computer/Laptop)

The page is divided into 12 columns.

#### Responsive Classes

Each class is articulated like this:

(Screen Size)-(Cols Number)

For example, for an item that takes 10 columns on a small screen and 5 columns otherwise :

```
<div class="col s10 m5 l5">
Your content
</div>
```

# Example
```
<body>
<div class="container>
<div class="row start">
Hello ! Im align to start !
<div class="col s12">
Your content !
</div>
</div>
<div class="row center">
Hello ! Im align to center !
<div class="col s12">
Your content !
</div>
</div>
<div class="row end">
Hello ! Im align to end !
<div class="col s12">
Your content !
</div>
</div>
</body>
```
