# Piperade Grid
## Une grille CSS simple propulsée par flexbox

### Installation
Télécharger la dernière version : [CSS (Compilé)](https://framagit.org/piperade/grid/-/jobs/artifacts/master/download?job=compile), [SCSS (Source)](https://framagit.org/piperade/grid/-/raw/master/grille.scss?inline=false)  
[Read in english](README.md)

### Classes disponibles
* `container`, pour contenir toute la page
* `row`, pour contenir les colonnes
* `col`, pour contenir le contenu
* `center`, centrer le contenu d'une ligne
* `start`, aligner au "départ" de la page le contenu d'une ligne
* `end`, align à la fin de la page le contenu d'une ligne

### Responsive Web Design

#### Plusieurs tailles d'écrans
* S : <= 600px (Smartphone)
* M : > 600px (Tablette)
* L : > 992px (Ordinateur/Laptop)

La page est divisée en 12 colonnes

#### Classes responsives

Les classes sont articulées comme ceci :

(Taille de l'écran)-(Nombre de colonnes)

Par exemple, pour un contenu qui prend 10 colonnes sur un petit écran et 5 colonnes autrement :

```
<div class="col s10 m5 l5">
Your content
</div>
```

# Example
```
<body>
<div class="container>
<div class="row start">
Hello ! Im align to start !
<div class="col s12">
Your content !
</div>
</div>
<div class="row center">
Hello ! Im align to center !
<div class="col s12">
Your content !
</div>
</div>
<div class="row end">
Hello ! Im align to end !
<div class="col s12">
Your content !
</div>
</div>
</body>
```
